Add `multigit.sh` to ~/.bashrc.

Create `git-checkenv.sh` alias on git using the following command:

```
git config --global alias.checkeven '!sh /path/to/git-checkeven.sh'
```
Multigit will search for repositories up to 4 directories below your current location.


Validate multiple repositories at once using multigit and git-checkeven together:

```
multigit git checkeven branch1 branch2 >> output 2> /dev/null
```



